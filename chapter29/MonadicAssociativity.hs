module MonadicAssociativity where

main :: IO ()
-- main = (putStrLn "1" >> putStrLn "2") >> putStrLn "3"
main = putStrLn "1" >> (putStrLn "2" >> putStrLn "3")
