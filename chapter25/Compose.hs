{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Compose where

-- https://hackage.haskell.org/package/base-4.9.1.0/docs/src/Data.Functor.Compose.html#Compose
newtype Compose f g a =
  Compose { getCompose :: f (g a) }
  deriving (Eq, Show)

instance (Functor f, Functor g) => Functor (Compose f g) where
  fmap f (Compose fga) = Compose $ (fmap . fmap) f fga

instance forall f g .
         (Applicative f, Applicative g) =>
         Applicative (Compose f g) where
  pure x = Compose (pure (pure x))

  (<*>) :: forall a b .
           Compose f g (a -> b)
        -> Compose f g a -- f (g a)
        -> Compose f g b -- f (g b)
  Compose f <*> Compose x =
    let liftApply :: f (g (a -> b))
                  -> f (g a -> g b)
        liftApply func = (<*>) <$> func

        apF :: f (g a -> g b)
        apF = liftApply f

        apApF :: f (g a) -> f (g b)
        apApF = (<*>) apF
     in Compose (apApF x)

instance forall f g .
         (Foldable f, Foldable g) =>
         Foldable (Compose f g) where
  foldMap :: forall a b . Monoid b => (a -> b) -> Compose f g a -> b
  foldMap f (Compose fga) = foldMap (foldMap f) fga

instance forall f g .
         (Traversable f, Traversable g) =>
         Traversable (Compose f g) where
  traverse :: forall a b z . (Applicative z) => (a -> z b) -> Compose f g a -> z (Compose f g b)
  traverse f (Compose fga) = Compose <$> traverse (traverse f) fga


-- fmap (+1) (Compose [Just 1, Nothing])
-- (Compose [Just (+1)]) <*> (Compose [Just 1, Nothing])
-- traverse Just (Compose ["abc", ""]) = Just (Compose {getCompose = ["abc",""]})
