{-# LANGUAGE BangPatterns #-}

module BangTest where

-- x :: a
-- x = undefined

-- y :: String
-- y = "blah"

-- main :: IO ()
-- main = do
--   print (snd (x, y))

tooLarge :: Int -> a
tooLarge _ = errorWithoutStackTrace ("!!: index too large")

--     x    partially apt go
fun :: a -> (Int -> a) -> Int -> a
fun = undefined

test :: [a] -> Int -> a
test xs n = foldr (\x r k -> case k of
                              0 -> x
                              _ -> r (k-1)) tooLarge xs n
