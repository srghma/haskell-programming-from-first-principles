module Test where

import Data.List
import Data.Function ((&))
import Control.Arrow ((>>>))

rvrs :: String -> String
-- application
-- rvrs str = str & words & reverse & unwords
-- rvrs str = unwords $ reverse $ words $ str
-- composition
-- rvrs = words >>> reverse >>> unwords
-- rvrs = unwords . reverse . words

main :: IO ()
main = print (rvrs "Curry is awesome!")


