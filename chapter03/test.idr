module Main

import Data.List

infixl 1 &
(&) : a -> (a -> b) -> b
x & f = f x

infixl 1 >>>
(>>>) : (a -> b) -> (b -> c) -> a -> c
(>>>) = flip (.)

rvrs : String -> String
-- application
-- rvrs str = str & words & reverse & unwords
-- rvrs str = unwords $ reverse $ words $ str
-- composition
-- rvrs = words >>> reverse >>> unwords
-- rvrs = unwords . reverse . words

main : IO ()
main = putStrLn $ rvrs "Curry is awesome!"
