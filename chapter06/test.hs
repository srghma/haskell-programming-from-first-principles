module Test where

type Subject = String
type Verb = String
type Object = String

data Sentence =
  Sentence Subject Verb Object
  deriving (Eq, Show)

s1 = Sentence "asd" "asdf"
s2 = Sentence "asd" "asdf" "asdf"
