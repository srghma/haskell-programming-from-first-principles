module Main where

tensDigit :: Integral a => a -> a
tensDigit x = tens `div` 10
  where (tens, _) = x `divMod` 10

foldBool2 :: a -> a -> Bool -> a
foldBool2 x y bool
  | bool      = x
  | otherwise = y

roundTrip :: (Show a, Read a) => a -> a
roundTrip = read . show

roundTrip2 :: (Show a, Read b) => a -> b
roundTrip2 = read . show

main = do
  print . roundTrip $ 3
  print ((roundTrip2 (3 :: Int)) :: Int)
