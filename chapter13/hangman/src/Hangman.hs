module Hangman
    ( runGame
    ) where

import Data.Char (toLower)
import Data.List (intersperse)
import System.Exit (exitSuccess)
import Control.Monad (when)

-- Word ├────────────────────────────────────────────────────────────────────

type IsGuessed = Bool
newtype Word = Word [(Char, IsGuessed)]

mkWord :: String -> Hangman.Word
mkWord w =
  let w' = fmap (\ch -> (toLower ch, False)) w
   in Hangman.Word w'

underscoreUnlessGuessed :: (Char, IsGuessed) -> Char
underscoreUnlessGuessed (ch, True) = ch
underscoreUnlessGuessed (_, False) = '_'

instance Show Hangman.Word where
  show (Word ws) = intersperse ' ' $ fmap underscoreUnlessGuessed ws

allCharsGuessed :: Hangman.Word -> Bool
allCharsGuessed (Word ws) = and $ fmap snd ws

getWord :: Hangman.Word -> String
getWord (Word ws) = fmap fst ws

charInWord :: Hangman.Word -> Char -> Bool
charInWord w ch = elem ch $ getWord w

flagAsGuessed :: Hangman.Word -> Char -> Hangman.Word
flagAsGuessed (Word ws) guess = Word $ fmap flagAsGuessed' ws
  where flagAsGuessed' :: (Char, IsGuessed) -> (Char, IsGuessed)
        flagAsGuessed' w@(ch, _) = if ch == guess then (ch, True) else w

-- Game ├────────────────────────────────────────────────────────────────────

type InputHistory = String

data Game = Game { hword :: Hangman.Word
                 , history :: InputHistory
                 , counter :: Int }

mkGame :: String -> Game
mkGame w = Game (mkWord w) [] 7

alreadyGuessed :: Game -> Char -> Bool
alreadyGuessed (Game _ h _) char = char `elem` h

correctGuess :: Game -> Char -> Game
correctGuess game@(Game w h _) ch = game { hword = flagAsGuessed w ch, history = ch:h }

incorrectGuess :: Game -> Char -> Game
incorrectGuess game@(Game _ h c) ch = game { history = ch:h, counter = c - 1 }

handleGuess :: Game -> Char -> IO Game
handleGuess game@(Game w _ _) guess = do
  putStrLn $ "Your guess was: " ++ [guess]
  case (charInWord w guess, alreadyGuessed game guess) of
       (_, True) -> do
         putStrLn "You already guessed that character, pick something else!"
         return game
       (True, _) -> do
         putStrLn "This character was in the word, filling the word accordingly"
         return (correctGuess game guess)
       (False, _) -> do
         putStrLn "This character wasn't in the word, try again."
         return (incorrectGuess game guess)


gameOver :: Game -> IO ()
gameOver (Game w _ c) =
  when (c <= 0) $ do
    putStrLn "You lose!"
    putStrLn $ "The word was: " ++ getWord w
    exitSuccess

gameWin :: Game -> IO ()
gameWin (Game w _ _) =
  when (allCharsGuessed w) $ do
    putStrLn "You win!"
    exitSuccess

go :: Game -> IO ()
go game = do
  gameWin game
  gameOver game
  putStrLn $ "Current puzzle is: " ++ show (hword game)
  putStr "Guess a letter: "
  guess <- getLine
  case guess of
       [c] -> handleGuess game c >>= go
       _   -> putStrLn "Your guess must be a single character"

runGame :: String -> IO ()
runGame word = go (mkGame word)
