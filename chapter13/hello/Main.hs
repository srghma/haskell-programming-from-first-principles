module Main where

import           Hello
import           System.IO

-- main :: IO Bool
-- main = do
--   c <- getChar
--   c' <- getChar
--   return (c == c')

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  putStr "Please input your name: "
  name <- getLine
  sayHello name
