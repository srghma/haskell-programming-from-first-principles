module Exercises where
-- {-# LANGUAGE ViewPatterns #-}

import           Test.Hspec
import           Test.QuickCheck
import           Test.QuickCheck.Function


--
newtype Identity a = Identity a deriving (Eq, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity (f a)

instance Arbitrary a => Arbitrary (Identity a) where
  arbitrary = do
    a <- arbitrary
    return $ Identity a

--
data Pair a = Pair a a deriving (Eq, Show)

instance Functor Pair where
  fmap f (Pair a b) = Pair (f a) (f b)

instance Arbitrary a => Arbitrary (Pair a) where
  arbitrary = do
    a <- arbitrary
    return $ Pair a a

--
functorCompose' :: (Eq (f c), Functor f) =>
                    f a
                 -> Fun a b
                 -> Fun b c
                 -> Bool
functorCompose' x (Fun _ f) (Fun _ g) =
  fmap (g . f) x == (fmap g . fmap f $ x)

type FunId x = Fun x x
type SomeFunctorComp a b = a b -> FunId b -> FunId b -> Bool

main :: IO ()
main = hspec $ do
  it "Identity a" $ property
    (functorCompose' :: SomeFunctorComp Identity Int)

  it "Pair a" $ property
    (functorCompose' :: SomeFunctorComp Pair Int)
