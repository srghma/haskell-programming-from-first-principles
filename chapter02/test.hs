module Test where

sayHello :: String -> IO ()
sayHello x = putStrLn ("hello, " ++ x ++ "!")

triple :: Num a => a -> a
triple x = x * 3

printInc x = print plusTwo
 where plusTwo = x + 2

-- take 7 . drop 9 $ "Curry is awesome!"
-- or
-- import Data.Function
-- "Curry is awesome!" & drop 9 & take 7
