module Test
import Data.Vect

-- -- using index func
-- Data.Vect.index (FS (FS FZ)) (2::3::4::Nil)
-- Data.Vect.index 2 (2::3::4::Nil)

-- -- constraining position with Elem
-- testVec : Vect 4 Int
-- testVec = 3 :: 4 :: 5 :: 6 :: Nil
-- -- or
-- testVec = [3, 4, 5, 6]
-- inVect : Elem 5 Test.testVec
-- inVect = There (There Here)

-- -- applicative add
-- m_add : Maybe Int -> Maybe Int -> Maybe Int
-- m_add x y = pure (+) <*> x <*> y
-- -- or
-- m_add x y = [| x + y |]
-- usage
-- m_add (Just 2) Nothing

-- -- using where
-- -- work
-- printInc : (Show x, Num x) => x -> IO ()
-- printInc x = let plusTwo = x + 2
--              in print plusTwo

-- -- dont work
-- printInc : (Show x, Num x) => x -> IO ()
-- printInc x = print plusTwo
--  where plusTwo = x + 2

-- triple : Num a => a -> a
-- triple x = x * 3

