{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Moi where

import           Control.Arrow (first)
import           Test.Hspec

newtype Moi s a =
  Moi { runMoi :: s -> (a, s) }

-- instance Functor (Moi s) where
--   fmap :: (a -> b) -> Moi s a -> Moi s b
--   fmap f (Moi sas) = Moi $
--     \s -> (f a', s')
--       where (a', s') = sas s
instance Functor (Moi s) where
  fmap :: (a -> b) -> Moi s a -> Moi s b
  fmap f (Moi sas) = Moi $ \s ->
    (\(a', s') -> (f a', s')) (sas s)

instance Applicative (Moi s) where
  pure :: a -> Moi s a
  pure a = Moi $ \s -> (a, s)

  (<*>) :: Moi s (a -> b)
        -> Moi s a
        -> Moi s b
  (Moi sab) <*> (Moi sa) = Moi $ \s ->
    let (ab, s') = sab s
        (a'', s'') = sa s'
     in (ab a'', s'')

instance Monad (Moi s) where
  return = pure
  (>>=) :: Moi s a
        -> (a -> Moi s b)
        -> Moi s b
  (Moi sa) >>= f = Moi $ \s ->
    let (a', s') = sa s
     in runMoi (f a') s'

get :: Moi s s
get = Moi $ \s -> (s, s)

main :: IO ()
main = hspec $ do
  let moi = Moi $ \s -> (0, s)
      fmoi = Moi $ \s -> ((+1), s)
      waitinga = \a -> Moi $ const (a+1, 9001)
  it "Functor" $
    runMoi ((+1) <$> moi) 0 `shouldBe` (1, 0)
  it "Applicative" $
    runMoi (fmoi <*> moi) 0 `shouldBe` (1, 0)
  it "Monad" $
    runMoi (moi >>= waitinga) 0 `shouldBe` (1, 9001)
  it "get" $
    runMoi get "curryIsAmaze" `shouldBe` ("curryIsAmaze","curryIsAmaze")
