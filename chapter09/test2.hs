module My where

import Test.Hspec

myReverse :: [a] -> [a]
myReverse = foldl (flip (:)) []

mySquish :: [[a]] -> [a]
mySquish [] = []
mySquish (x:xs) = x ++ mySquish xs

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy _ [] = undefined
myMaximumBy _ [x] = x
myMaximumBy f (x1:x2:xs) = case f x1 x2 of
                             GT -> x1
                             EQ -> x1
                             LT -> myMaximumBy f (x2:xs)

main :: IO ()
main = hspec $ do
  describe "myReverse" $ do
    it "correct" $ do
      let given = myReverse "blah"
          expected = "halb"
       in given `shouldBe` expected

  describe "mySquish" $ do
    it "correct" $ do
      let given = mySquish ["blah", "asdf"]
          expected = "blahasdf"
       in given `shouldBe` expected

  describe "myMaximumBy" $ do
    it "correct" $ do
      let given = myMaximumBy (\_ _ -> GT) [1..10]
          expected = 1
       in given `shouldBe` expected

    it "correct" $ do
      let given = myMaximumBy (\_ _ -> LT) [1..10]
          expected = 10
       in given `shouldBe` expected

    it "correct" $ do
      let given = myMaximumBy compare [1..10]
          expected = 10
       in given `shouldBe` expected

