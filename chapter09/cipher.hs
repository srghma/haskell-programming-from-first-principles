module My where

import Test.Hspec
import Data.Char

alphabetCount :: Int
alphabetCount = 26

aPos :: Int
aPos = ord 'A'

alphabetPos :: Char -> Int
alphabetPos ch = (ord ch) - aPos

unalphabetPos :: Int -> Char
unalphabetPos pos = chr $ (pos `mod` alphabetCount) + aPos

caesar :: Int -> String -> String
caesar pkey = fmap $ caesar' pkey
  where
    caesar' :: Int -> Char -> Char
    caesar' pkey' = unalphabetPos . (+pkey') . alphabetPos

main :: IO ()
main = hspec $ do
  it "correct" $ do
    given `shouldBe` expected
      where key      = 23
            plain    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            cipher   = "XYZABCDEFGHIJKLMNOPQRSTUVW"
            given    = caesar key plain
            expected = cipher
