module My where

import Test.Hspec
import Test.QuickCheck

myWords :: String -> [String]
myWords [] = []
myWords (' ':xs) = myWords xs
myWords str = word : myWords rest
  where
    isSpace :: Char -> Bool
    isSpace = (== ' ')

    notSpace :: Char -> Bool
    notSpace = not . isSpace

    (word, rest) = span notSpace str

main :: IO ()
main = hspec $ do
  it "correct" $ do
    given `shouldBe` expected
      where given    = myWords "all i wanna do is have some fun"
            expected = ["all","i","wanna","do","is","have","some","fun"]

