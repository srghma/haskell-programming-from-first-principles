module Main where


myreverse :: [a] -> [a]
myreverse [] = []
myreverse (x:xs) = myreverse xs ++ [x]

main :: IO ()
main = print $ myreverse [1, 2, 3]
