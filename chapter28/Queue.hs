module DList where

import           Criterion.Main
import           Data.Sequence

data Queue a =
  Queue { enqueue :: [a]
        , dequeue :: [a]
        } deriving (Eq, Show)

-- adds an item
push :: a -> Queue a -> Queue a
push = undefined

pop :: Queue a -> Maybe (a, Queue a)
pop = undefined

main :: IO ()
main = defaultMain
  [ bench "concat list" $ nf schlemiel 123
  ]
