module ReaderMaybeDifferenceTest where

-- Consider ReaderT r Maybe and MaybeT (Reader r) — are these
-- types equivalent? Do they do the same thing? Try writing otherwise
-- similar bits of code with each and see if you can prove they’re
-- the same or different.

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Maybe

type T1 r a = ReaderT r Maybe a -- r -> Maybe a
type T2 r a = MaybeT (Reader r) a -- r -> Identity (Maybe a)

-- tick :: T1 Int Int
-- tick = do n <- ask
--           return (n + 1)

-- plusOne :: Int -> Maybe Int
-- plusOne = runReaderT tick


tick :: T2 Int Int
tick = MaybeT $ do
        n <- ask
        return (Just (n + 1))

plusOne :: Int -> Maybe Int
plusOne = runReader . runMaybeT $ tick

main = print $ plusOne 1

