{-# LANGUAGE ScopedTypeVariables #-}

module EitherT where

newtype EitherT e m a =
  EitherT { runEitherT :: m (Either e a) }

instance Functor m => Functor (EitherT e m) where
  fmap f (EitherT mEea) = EitherT $ (fmap . fmap) f mEea

instance Applicative m => Applicative (EitherT e m) where
  pure = EitherT . pure . pure
  (EitherT mEaf) <*> (EitherT mEea) = EitherT $ (<*>) <$> mEaf <*> mEea

instance Monad m => Monad (EitherT e m) where
  return = pure
  (EitherT mEea) >>= f =
    EitherT $ do
      ea <- mEea
      case ea of
        Left e  -> return (Left e)
        Right a -> runEitherT (f a)

swapEitherT :: forall e m a . (Functor m) => EitherT e m a -> EitherT a m e
swapEitherT (EitherT mEea) = EitherT $ fmap swapEitherT' mEea
  where swapEitherT' :: Either e a -> Either a e
        swapEitherT' (Left e)  = Right e
        swapEitherT' (Right a) = Left a

eitherT :: Monad m =>
           (e -> m b)
        -> (a -> m b)
        -> EitherT e m a
        -> m b
eitherT emb amb (EitherT mEea) = do
  eea <- mEea
  case eea of
    Left e  -> emb e
    Right a -> amb a
