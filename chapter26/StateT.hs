{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- how to be friends with compilator - "The bind breakdown" on pg 912:
module StateT where

import           Control.Arrow         (first)
import           Data.Functor.Identity
import           Test.Hspec

newtype StateT s m a =
  StateT { runStateT :: s -> m (a, s) }

instance forall s m . Functor m => Functor (StateT s m) where
  fmap :: forall a b . (a -> b) -> StateT s m a -> StateT s m b
  fmap f (StateT sma) = StateT $ \s ->
    let mas = sma s
        go :: (a, s) -> (b, s)
        go = first f
     in fmap go mas

instance forall s m . Applicative m => Applicative (StateT s m) where
  pure a = StateT $ \s -> pure (a, s)

  -- first try, not actually correct (sma should get new state from func, through monad bind)
  -- https://stackoverflow.com/questions/18673525/is-it-possible-to-implement-applicative-m-applicative-statet-s-m
  -- correct http://hackage.haskell.org/package/transformers-0.5.4.0/docs/src/Control.Monad.Trans.State.Lazy.html#line-201
  (<*>) :: forall a b .
           StateT s m (a -> b)
        -> StateT s m a
        -> StateT s m b
  (StateT smf) <*> (StateT sma) = StateT $ \s ->
    let mf :: m (a -> b, s)
        mf = smf s
        app :: (a -> b, s) -> (a, s) -> (b, s)
        app fs = first (fst fs)
        mas' :: m (b, s)
        mas' = app <$> mf <*> sma s
     in mas'

instance forall s m . Monad m => Monad (StateT s m) where
  return = pure

  (>>=) :: forall a b .
           StateT s m a
        -> (a -> StateT s m b)
        -> StateT s m b
  (StateT sma) >>= f = StateT $ \s -> do
    (a', s') <- sma s
    runStateT (f a') s'

main :: IO ()
main = hspec $ do
  let state :: Num a => StateT s Maybe a
      state = StateT $ \s -> Just(0, s)
  it "Functor" $
    runStateT ((+1) <$> state) 0 `shouldBe` Just(1, 0)
