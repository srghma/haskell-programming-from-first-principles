module InnerIsOuterChapTest where

import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader

-- for visibility
returnIO :: a -> IO a
returnIO = pure

embedded :: MaybeT (ExceptT String (ReaderT () IO)) Int
embedded = go (const (Right (Just 1)))
  where go :: (() -> Either String (Maybe Int)) -> MaybeT (ExceptT String (ReaderT () IO)) Int
        go = MaybeT $ ExceptT $ ReaderT return

        mmm :: MaybeT IO Int
        mmm = MaybeT . returnIO . Just $ 1

        mmr :: MaybeT (ExceptT String IO) Int
        mmr = MaybeT $ ExceptT $ returnIO $ Right . Just $ 1

        mmb :: ReaderT () IO Int
        mmb = ReaderT $ const (returnIO 1)

        mmb' :: MaybeT (ReaderT () IO) Int
        mmb' = MaybeT $ ReaderT $ const (returnIO 1)

        mmb'' :: MaybeT (ExceptT String (ReaderT () IO)) Int
        mmb'' = MaybeT $ ExceptT $ ReaderT $ const (returnIO 1)
