module Morra where

import           Control.Monad             (forever)
import           Control.Monad.Trans.State (State)
import           Data.Default.Class
import           System.Exit               (exitSuccess)
import           System.Random             (randomRIO)
import           Text.Read                 (readMaybe)

data Player = Player
  { score   :: Integer
  , history :: [Integer]
  } deriving (Show,Eq)

instance Default Player where def = Player 0 []

data Game = Game
  { me       :: Player
  , computer :: Player
  } deriving (Show,Eq)

instance Default Game where def = Game def def

handleGuess :: String -> IO Int
handleGuess invitation = do
  putStr invitation
  guess <- getLine
  case readMaybe guess of
    Nothing -> tryAgain "Not integer, try again"
    Just n
      | invalid n -> tryAgain "Must be from 1 to 5, try again"
      | otherwise -> return n
  where tryAgain str = putStrLn str >> handleGuess invitation
        invalid n = n < 1 || n > 5

computerGuess :: String -> IO Int
computerGuess invitation = do
  guess <- randomRIO (1,5 :: Int)
  putStrLn $ invitation ++ show guess
  return guess

runGame :: Game -> IO ()
runGame g = forever $ do
  pGuess <- handleGuess "P: "
  cGuess <- computerGuess "C: "
  if pGuess > cGuess
     then putStrLn "- P wins"
     else putStrLn "- C wins"

main :: IO ()
main = do
  putStrLn "--p is Player"
  putStrLn "--c is Computer"
  runGame def
