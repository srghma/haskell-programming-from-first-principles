{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Exercises where

import           Data.Functor.Identity
import           Control.Monad.Trans.Reader
import           Control.Monad.Trans.State
import           Control.Monad.IO.Class (liftIO)
import           Test.Hspec

-- rDec :: Num a => Reader a a
-- rDec = do
--   n <- ask
--   return (n - 1)

rDec :: Num a => Reader a a
rDec = subtract 1 <$> ask

-- rShow :: Show a => ReaderT a Identity String
-- rShow = do
--   n <- ask
--   return (show n)

rShow :: Show a => ReaderT a Identity String
rShow = show <$> ask

rPrintAndInc :: (Num a, Show a) => ReaderT a IO a
rPrintAndInc = do
  a <- ask
  liftIO . putStrLn $ "Hi: " ++ show a
  return (a + 1)

sPrintIncAccum :: (Num a, Show a) => StateT a IO String
sPrintIncAccum = do
  a <- get
  put (a + 1)
  let a' = show a
  liftIO . putStrLn $ "Hi: " ++ a'
  return a'

main :: IO ()
main = hspec $ do
  it "rDec" $ do
    let given = fmap (runReader rDec) [1..10]
    let expct = [0,1,2,3,4,5,6,7,8,9]
    given `shouldBe` expct

  it "rShow" $ do
    let given = runReader rShow (1 :: Integer)
    let expct = "1"
    given `shouldBe` expct

  it "rPrintAndInc" $ do
    given <- traverse (runReaderT rPrintAndInc) [1..10 :: Integer]
    let expct = [2,3,4,5,6,7,8,9,10,11]
    given `shouldBe` expct

  it "sPrintIncAccum" $ do
    given <- mapM (runStateT sPrintIncAccum) [1..5 :: Integer]
    let expct = [("1",2),("2",3),("3",4),("4",5),("5",6)]
    given `shouldBe` expct

