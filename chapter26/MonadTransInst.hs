module MonadTransInst where

import Control.Monad.Trans.Class
-- import Control.Monad.Trans.Maybe
-- import Control.Monad.Trans.Either
-- import Control.Monad.Trans.State

newtype MaybeT m a = MaybeT { runMaybeT :: m (Maybe a) }
newtype EitherT e m a = EitherT { runEitherT :: m (Either e a) }
newtype StateT s m a = StateT { runStateT :: s -> m (a,s) }

instance MonadTrans MaybeT where

  -- lift :: (Monad m) => m a -> t m a
  -- (MaybeT . liftM Just) :: Monad m => m a -> MaybeT m a
  -- MaybeT :: m (Maybe a) -> MaybeT m a
  -- (liftM Just) :: Monad m => m a -> m (Maybe a)
  lift = MaybeT . fmap Just


-- lift - wrap some type with type m a
-- https://hackage.haskell.org/package/either-4.4.1.1/docs/src/Control.Monad.Trans.Either.html#line-262
instance MonadTrans (EitherT e) where
  lift = EitherT . fmap Right

instance MonadTrans (StateT s) where
  lift m = StateT $ \ s -> do
      a <- m
      return (a, s)
