-- TODO learn more
module ContTest where

import           Control.Monad
import           Control.Monad.Cont

----------------------------------
thrice :: (a -> a) -> a -> a
thrice f x = f (f (f x))

----------------------------------
thrice_cps :: (a -> ((a -> r) -> r)) -> a -> ((a -> r) -> r)
thrice_cps f_cps x = \k ->
  f_cps x $ \fx ->
  f_cps fx $ \ffx ->
  f_cps ffx $ k

tail_cps :: String -> ((String -> r) -> r)
tail_cps str = \f -> f $ tail str

----------------------------------
thrice_cont :: (a -> Cont r a) -> a -> Cont r a
thrice_cont f x = do
  fx <- f x
  fxx <- f fx
  f fxx

tail_cont :: [a] -> Cont r [a]
tail_cont x = return $ tail x

-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
main :: IO ()
main = do
  print $ thrice tail "foobar"
  thrice_cps tail_cps "foobar" print
  runCont (thrice_cont tail_cont "foobar") print



callcc :: Cont String Integer -- i take on input function that takes int and outp stirng
callcc = do
  a <- return 1
  -- in Cont (\k -> ...) you can either construct you own Cont,
  -- or use default one function-constructor (k) and thus exit from outer Cont
  b <- callCC (\k -> k 2)
  return $ a+b

ex2 :: IO ()
ex2 = print $ runCont callcc show
-- "3"

----------------------------
-- https://en.wikibooks.org/wiki/Haskell/Continuation_passing_style
divExcpt :: Int -> Int -> (String -> Cont r Int) -> Cont r Int
divExcpt x y handler = callCC $ \ok -> do
  err <- callCC $ \notOk -> do
    when (y == 0) $ notOk "Denominator 0"
    ok $ x `div` y
  handler err

{- For example,
runCont (divExcpt 10 2 error) id --> 5
runCont (divExcpt 10 0 error) id --> *** Exception: Denominator 0
-}

-- http://dev.stephendiehl.com/hask/#cont
-- runCont :: Cont r a -> (a -> r) -> r
-- newtype Cont r a = Cont { runCont :: ((a -> r) -> r) }

-- instance Monad (Cont r) where
--   return a       = Cont $ \k -> k a
--   (Cont c) >>= f = Cont $ \k -> c (\a -> runCont (f a) k)

-- class (Monad m) => MonadCont m where
--   callCC :: ((a -> m b) -> m a) -> m a --function which gives us back explicit control of continuations - but only where we want it
-- instance MonadCont (Cont r) where
--   callCC f = Cont $ \k -> runCont (f (\a -> Cont $ \_ -> k a)) k
