{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- to run - :main lol
module Main where

import           Control.Monad.IO.Class
import           Control.Monad.Trans.Class
import           Control.Monad.Trans.Reader
import           Data.IORef
import qualified Data.Map.Lazy              as M
import           Data.Text.Lazy             (Text)
import qualified Data.Text.Lazy             as TL
import           Network.Wai                (Response)
import           System.Environment         (getArgs)
import           Web.Scotty.Trans

data Config =
  Config {
    -- that's one, one click!
    -- two...two clicks!
    -- Three BEAUTIFUL clicks! ah ah ahhhh
    counts :: IORef (M.Map Text Integer)
  , prefix :: Text
  }

-- Stuff inside ScottyT is, except for things that escape
-- via IO, effectively read-only so we can't use StateT.
-- It would overcomplicate things to attempt to do so and
-- you should be using a proper database for production
-- applications.
type Scotty = ScottyT Text (ReaderT Config IO)
type Handler = ActionT Text (ReaderT Config IO)

bumpBoomp :: Text
          -> M.Map Text Integer
          -> (M.Map Text Integer, Integer)
bumpBoomp k m =
  case M.lookup k m of
    Nothing    -> (M.insert k 1 m, 1)
    Just count -> (M.insert k (count + 1) m, count + 1)

gets :: ReaderT Config IO (M.Map Text Integer)
gets = asks counts >>= liftIO . readIORef

app :: Scotty ()
app =
  get "/:key" go
  where go :: Handler ()
        go = do
          unprefixed <- param "key"
          pref <- lift $ asks prefix
          let key' = mappend pref unprefixed
          countsRef <- lift $ asks counts
          counts' <- liftIO . readIORef $ countsRef
          let (newMap, newInteger) = bumpBoomp key' counts'
          liftIO $ writeIORef countsRef newMap
          html $ mconcat [ "<h1>Success! Count was: "
                         , TL.pack $ show newInteger
                         , "</h1>"
                         ]

main :: IO ()
main = do
  [prefixArg] <- getArgs
  counter <- newIORef M.empty
  let config = Config counter (TL.pack prefixArg)
      runR :: ReaderT Config IO Response -> IO Response
      runR m = runReaderT m config
  scottyT 3000 runR app
