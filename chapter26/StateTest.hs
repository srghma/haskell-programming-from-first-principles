module StateTest where

import Control.Monad.State.Lazy
import Control.Monad.IO.Class (liftIO)
import Debug.Trace

-- https://stackoverflow.com/questions/44931886/how-to-print-intermediate-value-in-haskell

-- type Logger = String -> IO ()

-- tick :: Logger -> StateT Int IO Int
-- tick logger = do n <- get
--                  put (n+1)
--                  liftIO $ logger $ show n
--                  return n

-- plusOne :: Int -> IO Int
-- plusOne = execStateT (tick putStrLn)

-- main = plusOne 1 >> pure ()

tick :: State Int Int
tick = get >>=
       \n -> put (n+1) >>=
       \v -> traceShow (n, v) $ return n

plusOne :: Int -> Int
plusOne = execState tick

main = print $ plusOne 1

-- -------------
-- -- given
-- type State s = StateT s Identity

-- -- | Construct a state monad computation from a function.
-- -- (The inverse of 'runState'.)
-- state :: (Monad m)
--       => (s -> (a, s))  -- ^pure state transformer
--       -> StateT s m a   -- ^equivalent state-passing computation
-- state f = StateT (return . f)

-- get :: (Monad m) => StateT s m s
-- get = state $ \ s -> (s, s)

-- -- | @'put' s@ sets the state within the monad to @s@.
-- put :: (Monad m) => s -> StateT s m ()
-- put s = state $ \ _ -> ((), s)

-- execState :: State s a  -- ^state-passing computation to execute
--           -> s          -- ^initial value
--           -> s          -- ^final state
-- execState m s = snd (runState m s)

-- runState :: State s a   -- ^state-passing computation to execute
--          -> s           -- ^initial state
--          -> (a, s)      -- ^return value and final state
-- runState m = runIdentity . runStateT m

-- instance (Monad m) => Monad (StateT s m) where
--     return a = StateT $ \ s -> return (a, s)
--     {-# INLINE return #-}

--     m >>= k  = StateT $ \ s -> do
--         ~(a, s') <- runStateT m s
--         runStateT (k a) s'

-- --

-- -- XXX note IO accamulate effects like State, because its State inside
-- tick' :: State Int Int
-- tick'  = get >>=
--   \n -> put (n+1) >>=
--   \_ -> return n

-- tick' :: IO (State Int Int)
-- tick' = do n <- get
--           put (n+1)
--           return n

