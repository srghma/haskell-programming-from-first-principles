module LiftVsLiftIO where

-- https://stackoverflow.com/questions/3921237/haskell-lift-vs-liftio

import Control.Monad.Trans.Class
import Control.Monad.Trans.Writer
import Control.Monad.Trans.Reader

type T = ReaderT Int (WriterT String IO) Bool

let x :: T; x = undefined

-- which means insert x in structure T
--
-- > :t \x -> (lift x :: T)
-- \x -> (lift x :: T) :: WriterT String IO Bool -> T

-- > :t \x -> (liftIO x :: T)
-- \x -> (liftIO x :: T) :: IO Bool -> T
