module HutonsRazor where

import           Test.Hspec

data Expr
  = Lit Integer
  | Expr :+: Expr -- infix data constructors must start from :

eval :: Expr -> Integer
eval (Lit i)     = i
eval (le :+: re) = eval le + eval re

printExpr :: Expr -> String
printExpr (Lit i)     = show i
printExpr (le :+: re) = printExpr le ++ " + " ++ printExpr re

main :: IO ()
main = hspec $ do
  it "eval" $ do
    eval ((Lit 1) :+: (Lit 9001)) `shouldBe` 9002

  it "printExpr" $ do
    printExpr ((Lit 1) :+: (Lit 9001)) `shouldBe` "1 + 9001"
