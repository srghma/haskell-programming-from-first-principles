-- UNDONE
module My where

import           Data.Char
import           Test.Hspec

alphabetSize :: Int
alphabetSize = 26

aPos :: Int
aPos = ord 'A'

alphabetPos :: Char -> Int
alphabetPos ch = (ord ch) - aPos

unalphabetPos :: Int -> Char
unalphabetPos pos = chr $ (pos `mod` alphabetSize) + aPos

vigenere :: String -> String -> String
vigenere keyword plain =
  let alphabetPosUnlessSpace =
      keywordPositions = fmap alphabetPos keyword

      plainPositions   = fmap alphabetPos plain

      -- zip is right-lazy, so cycle on right side
      keywordPlain  = zip plainPositions (cycle keywordPositions)

      cipherUnlessSpace (pPos, kPos) =
        let spacePos = alphabetPos ' '
         in if pPos == spacePos
               then ' '
               else unalphabetPos $ kPos + pPos
      cipherText = fmap cipherUnlessSpace keywordPlain
   in
    cipherText

main :: IO ()
main = hspec $ do
  it "correct" $ do
    given `shouldBe` expected
      where keyword  = "ALLY"
            plain    = "MEET AT DAWN"
            cipher   = "MPPR AE OYWY"
            given    = vigenere keyword plain
            expected = cipher
