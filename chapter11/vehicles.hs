module Main where

data Price =
  Price Integer deriving (Eq, Show)


data Manufacturer =
  Mini
  | Mazda
  | Tata
  deriving (Eq, Show)


data Airline =
  PapuAir
  | CatapultsR'Us
  | TakeYourChancesUnited
  deriving (Eq, Show)

data Vehicle =
  Car Manufacturer Price
  | Plane Airline
  deriving (Eq, Show)

myCar = Car Mini (Price 14000)
urCar = Car Mazda (Price 20000)
clownCar = Car Tata (Price 7000)
doge = Plane PapuAir

isCar :: Vehicle -> Bool
isCar v = case v of
            Car _ _ -> True
            Plane _ -> False

isPlane :: Vehicle -> Bool
isPlane v = case v of
            Plane _ -> True
            Car _ _ -> False

areCars :: [Vehicle] -> [Bool]
areCars = all isCar

getManu :: Vehicle -> Maybe Manufacturer
getManu v = case v of
              Car manu _ -> Maybe manu
              Plane _    -> Nothing
