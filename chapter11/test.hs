module Test where


-- Id ├────────────────────────────────────────────────────────────────────

data Id a =
  MkId a deriving (Eq, Show)

idIdentity :: Id (a -> a)
idIdentity = MkId $ \x -> x


-- Automobile ├────────────────────────────────────────────────────────────────────

-- DONT DO THIS
-- use Maybe
data Automobile = Null
                | Car { make  :: String
                      , model :: String
                      , year  :: Integer }
                deriving (Eq, Show)

-- because
updateAuto :: String
updateAuto = make Null
