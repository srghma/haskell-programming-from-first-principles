-- shamelessly stolen
-- https://codereview.stackexchange.com/questions/115052/vigen%C3%A8re-cipher-in-haskell
module Vigenere where

import           Data.Bimap (Bimap, empty, insert, size, (!), (!>))
import           Data.List  (foldl')
import           Test.Hspec

type Alphabet = Bimap Int Char

mkAlphabet :: [Char] -> Alphabet
mkAlphabet = foldl (\alph ch -> insert (size alph) ch alph) empty
-- mkAlphabet = foldl (flip =<< insert . size) empty

safeIndex :: [a] -> Int -> a
safeIndex xs pos = xs !! (pos `mod` length xs)

isSpace :: Char -> Bool
isSpace = (==) ' '

-- NOTES
-- the task required to left spaces untouched, so
padKeyword :: String -> String -> String
padKeyword plain keyword =
  let padder :: (String, Int) -> Char -> (String, Int)
      padder (acc, keywordIndex) plainCh =
        if isSpace plainCh
           then (' ' : acc, keywordIndex)
           else (safeIndex keyword keywordIndex : acc, keywordIndex + 1)
   in reverse $ fst $ foldl' padder ("", 0) plain

-- -- NOTES
-- -- zipWith right-lazy
-- -- never use foldl (useless for infinite lists), use foldr or foldl'
encrypt :: Alphabet -> [Char] -> [Char] -> [Char]
encrypt alphabet keyword plain =
  let encrypt' :: Char -> Char -> Char
      encrypt' plainCh keywordCh =
        if isSpace plainCh -- left spaces untouched
          then ' '
          else alphabet ! boundedEncryptedChPos
        where encryptedChPos = (alphabet !> plainCh) + (alphabet !> keywordCh)
              boundedEncryptedChPos = encryptedChPos `mod` size alphabet
   in zipWith encrypt' plain $ padKeyword plain keyword


main :: IO ()
main = hspec $ do
  it "padKeyword correct" $ do
    let given    = padKeyword "MEET AT DAWN" "ALLY"
        expected = "ALLY AL LYAL"
     in given `shouldBe` expected

  it "encrypt correct" $ do
    let alphabet = mkAlphabet $ ['A' .. 'Z']
        keyword  = "ALLY"
        plain    = "MEET AT DAWN"
        cipher   = "MPPR AE OYWY"

        given    = encrypt alphabet keyword plain
        expected = cipher
     in given `shouldBe` expected
