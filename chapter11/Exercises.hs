module Exercises where

import           Data.Char  (toUpper)
import           Test.Hspec

isSubsequenceOf :: (Eq a) => [a] -> [a] -> Bool
isSubsequenceOf [] _ = True
isSubsequenceOf _ [] = False
isSubsequenceOf allxs@(x:xs) allys@(y:ys) =
  if x == y
     then isSubsequenceOf xs ys
     else isSubsequenceOf allxs ys

capitalizeWords :: String -> [(String, String)]
capitalizeWords str =
  let ws = words str
      capitalize' :: String -> (String, String)
      capitalize' ""            = ("", "")
      capitalize' word@(ch:chs) = (word, toUpper ch : chs)
   in map capitalize' ws

main :: IO ()
main = hspec $ do
  it "isSubsequenceOf" $ do
    isSubsequenceOf "blah" "blahwoot" `shouldBe` True
    isSubsequenceOf "blah" "wootblah" `shouldBe` True
    isSubsequenceOf "blah" "wboloath" `shouldBe` True
    isSubsequenceOf "blah" "wootbla"  `shouldBe` False

  it "capitalizeWords" $ do
    capitalizeWords "hello world" `shouldBe` [("hello", "Hello"), ("world", "World")]
