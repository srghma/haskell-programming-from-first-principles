{-# LANGUAGE FlexibleInstances #-}

module TooMany where

import           Data.Maybe
import           Test.Hspec
import           Text.Read


class TooMany a where
  tooMany :: a -> Bool

instance TooMany Int where
  tooMany n = n > 42

instance TooMany (Int, String) where
  tooMany (n, s) =
    let s' :: Int
        s' = fromMaybe 0 $ readMaybe s
     in tooMany $ n + s'

instance TooMany (Int, Int, String) where
  tooMany (a, b, c) = tooMany a || tooMany (b, c)

main :: IO ()
main = hspec $ do
  describe "tooMany" $ do
    it "correct" $ do
      let given = tooMany (1 :: Int, 1 :: Int, "42")
          expected = True
       in given `shouldBe` expected
