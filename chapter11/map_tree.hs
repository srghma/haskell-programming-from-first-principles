module MapTree where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' b Leaf = Node Leaf b Leaf
insert' b (Node left a right)
  | b == a = Node left a right
  | b < a = Node (insert' b left) a right
  | b > a = Node left a (insert' b right)

foldTree :: (a -> b -> b) -> b -> BinaryTree a -> b
foldTree f acc Leaf                = acc
foldTree f acc (Node left a right) = acc_left
  where
    acc_left   = foldTree f acc_middle left
    acc_middle = f a acc_right
    acc_right  = foldTree f acc right

mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
mapTree _ Leaf = Leaf
mapTree f (Node left a right) =
  Node (mapTree f left) (f a) (mapTree f right)

-- ITS IMPOSSIBLE TO WRITE MAPTREE VIA FOLDTREE AND NOT USE INSERT
-- mapTree :: (a -> b) -> BinaryTree a -> BinaryTree b
-- mapTree f = foldTree mapper Leaf
--   where mapper :: a -> b -> b
--         mapper prev_a acc = case acc of
--           Leaf                -> Leaf
--           (Node left a right) -> Node left (f a) right

testTree' :: BinaryTree Integer
testTree' =
  Node (Node Leaf 3 Leaf) 1 (Node Leaf 4 Leaf)

mapExpected =
  Node (Node Leaf 4 Leaf) 2 (Node Leaf 5 Leaf)

mapOkay =
  if mapTree (+1) testTree' == mapExpected
  then print "yup okay!"
  else error "test failed!"
