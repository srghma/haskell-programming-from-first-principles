module Test where

import           Test.Hspec

myIterate :: (a -> a) -> a -> [a]
myIterate f initial = initial : myIterate f init'
  where init' = f initial

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f initial = case init' of
                     Nothing           -> []
                     Just (prev, next) -> prev : myUnfoldr f next
  where init' = f initial

betterIterate :: (a -> a) -> a -> [a]
betterIterate f initial = myUnfoldr (\b -> Just (b, f b)) initial

main :: IO ()
main = hspec $ do
  it "myIterate" $ do
    let given = take 10 $ myIterate (+1) 0
        expected = [0,1,2,3,4,5,6,7,8,9]
     in given `shouldBe` expected

  it "betterIterate" $ do
    let given = take 10 $ betterIterate (+1) 0
        expected = [0,1,2,3,4,5,6,7,8,9]
     in given `shouldBe` expected
