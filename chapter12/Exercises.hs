module Exercises where

import           Data.Maybe (fromMaybe)
import           Test.Hspec

-- Linguist exercises ├────────────────────────────────────────────────────────────────────
-- >>> notThe "the"
-- Nothing
-- >>> notThe "blahtheblah"
-- Just "blahtheblah"
-- >>> notThe "woot"
-- Just "woot"
notThe :: String -> Maybe String
notThe "the" = Nothing
notThe word  = Just word

-- >>> replaceThe "the cow loves us"
-- "a cow loves us"
replaceThe :: String -> String
replaceThe = unwords . fmap (fromMaybe "a") . map notThe . words

-- Validate the word ├────────────────────────────────────────────────────────────────────

newtype Word' =
  Word' String
  deriving (Eq, Show)

vowels = "aeiou"

mkWord :: String -> Maybe Word'
mkWord word = let incIfVowel chr acc = if chr `elem` vowels then acc + 1 else acc
                  vowelsCount = foldr incIfVowel 0 word
                  consonantsCount = length word - vowelsCount
                  isOk = vowelsCount > consonantsCount
               -- in if isOk then Just (Word' word) else Nothing
               in if isOk then Just (Word' word) else Nothing

-- Nat ├────────────────────────────────────────────────────────────────────

-- As natural as any competitive bodybuilder
data Nat =
    Zero
  | Succ Nat
  deriving (Eq, Show)

-- >>> natToInteger Zero
-- 0
-- >>> natToInteger (Succ Zero)
-- 1
-- >>> natToInteger (Succ (Succ Zero))
-- 2
natToInteger :: Nat -> Integer
natToInteger Zero       = 0
natToInteger (Succ nat) = 1 + natToInteger nat

-- >>> integerToNat 0
-- Just Zero
-- >>> integerToNat 1
-- Just (Succ Zero)
-- >>> integerToNat 2
-- Just (Succ (Succ Zero))
-- >>> integerToNat (-1)
-- Nothing
integerToNat :: Integer -> Maybe Nat
integerToNat n
  | n < 0 = Nothing
  | otherwise = Just (integerToNat' n)
  where integerToNat' :: Integer -> Nat
        integerToNat' 0 = Zero
        integerToNat' n = Succ (integerToNat' (n - 1))

-- Small library for Either ├────────────────────────────────────────────────────────────────────

lefts' :: [Either a b] -> [a]
lefts' = foldr lefts'' []
  where lefts'' e acc = case e of
                          Left x  -> x:acc
                          Right _ -> acc

main :: IO ()
main = hspec $ do
  it "lefts'" $ do
    lefts' [ Left "foo", Right 3, Left "bar", Right 7, Left "baz" ] `shouldBe` ["foo","bar","baz"]
