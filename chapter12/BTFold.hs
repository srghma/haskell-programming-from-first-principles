module Test where

import           Test.Hspec

-- BT ├────────────────────────────────────────────────────────────────────

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

-- treeBuild :: Integer -> BinaryTree Integer
-- treeBuild maxN =
--   let treeBuild' currentN =
--         let nextN = currentN + 1
--             nextTree = treeBuild' nextN
--          in if currentN >= maxN
--               then Leaf
--               else Node nextTree currentN nextTree
--    in treeBuild' 0

unfold :: (a -> Maybe (a,b,a)) -> a -> BinaryTree b
unfold f initial =
  let build' inserted next =
        let nextTree = unfold f next
         in Node nextTree inserted nextTree
   in case f initial of
        Nothing                     -> Leaf
        Just (prev, inserted, next) -> build' inserted next

treeBuild :: Integer -> BinaryTree Integer
treeBuild maxN =
  let treeBuild' :: Integer -> Maybe (Integer, Integer, Integer)
      treeBuild' currentN = if currentN >= maxN
                              then Nothing
                              else Just (currentN, currentN, currentN + 1)
   in unfold treeBuild' 0

main :: IO ()
main = hspec $ do
  it "treeBuild" $ do
    let given = treeBuild 0
        expected = Leaf
     in given `shouldBe` expected

  it "treeBuild" $ do
    let given = treeBuild 1
        expected = Node Leaf 0 Leaf
     in given `shouldBe` expected

  it "treeBuild" $ do
    let given = treeBuild 3
        expected = Node (Node (Node Leaf 2 Leaf)
                               1
                               (Node Leaf 2 Leaf))
                        0
                        (Node (Node Leaf 2 Leaf)
                               1
                               (Node Leaf 2 Leaf))
     in given `shouldBe` expected
