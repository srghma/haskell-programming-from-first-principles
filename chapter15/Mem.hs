module Mem where

import Data.Monoid
import Test.Hspec

newtype Mem s a =
  Mem { runMem :: s -> (a,s) }

instance Monoid a => Monoid (Mem s a) where
  mempty = Mem $ \n -> (mempty, n)
  mappend (Mem f) (Mem g) = 
    let (fA, fS) = f n

     in Mem $ \n -> f n `mappend` g n

f' :: Mem Integer String
f' = Mem $ \s -> ("hi", s + 1)

main :: IO ()
main = hspec $
  describe "Mem" $
    it "Mem" $ do
      runMem (f' <> mempty) 0 `shouldBe` ("hi", 1)
      runMem (mempty <> f') 0 `shouldBe` ("hi", 1)
      (runMem mempty 0 :: (String, Int)) `shouldBe` ("", 0)
      runMem (f' <> mempty) 0 == runMem f' 0 `shouldBe` True
      runMem (mempty <> f') 0 == runMem f' 0 `shouldBe` True
