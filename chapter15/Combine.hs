module Exercises where

import           Test.Hspec
import           Data.Semigroup
import           Data.Monoid hiding ((<>))
import Test.QuickCheck
import Test.QuickCheck.Function

newtype Combine a b =
  Combine { unCombine :: a -> b }

instance Semigroup b => Semigroup (Combine a b) where
  (Combine f) <> (Combine g) = Combine $ \n -> f n <> g n

instance (Monoid b, Semigroup b) => Monoid (Combine a b) where
  mempty = Combine mempty
  mappend = (<>)

instance Arbitrary a => Arbitrary (Combine a) where
  arbitrary = do
    a <- arbitrary
    return $ Combine a

--helpers
semigroupAssoc :: (Eq m, Semigroup m) => x -> Fun x m -> Fun x m -> Fun x m -> Bool
semigroupAssoc x a b c = (a <> (b <> c)) == ((a <> b) <> c)

type FunId x = Fun x x
type SomeFunctorComp a b = a b -> FunId b -> FunId b -> Bool

-- tests
main :: IO ()
main = hspec $ do
  describe "Combine Semigroup" $ do
    it "combine" $
      let f = Combine $ \n -> Sum (n + 1)
          g = Combine $ \n -> Sum (n - 1)
       in do unCombine (f <> g) 0 `shouldBe` Sum 0
             unCombine (f <> g) 1 `shouldBe` Sum 2
             unCombine (f <> f) 1 `shouldBe` Sum 4
             unCombine (g <> f) 1 `shouldBe` Sum 2
    it "with QuickCheck" $
      property (semigroupAssoc :: SomeAssoc CombineToSum)

  describe "Combine Monoid" $
    it "combine" $
      let f = Combine $ \n -> Sum (n + 1)
       in unCombine (mappend f mempty) 1 `shouldBe` Sum 2
