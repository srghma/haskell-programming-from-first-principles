{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TraversList where

-- from https://ocharles.org.uk/blog/guest-posts/2014-12-15-deriving.html
data List a = Nil | Cons a (List a) deriving (Eq, Show, Functor)

instance Foldable List where
  foldr _ z Nil = z
  foldr f z (Cons x xs) = f x $ foldr f z xs

instance Traversable List where
  -- traverse f (Cons x xs) = Cons <$> f x <*> traverse f xs

  traverse :: forall f a b . (Applicative f) => (a -> f b) -> List a -> f (List b)
  traverse _ Nil = pure Nil
  traverse f (Cons x xs) = lifted x <*> traverse f xs
    where lifted :: a -> f (List b -> List b)
          lifted x' = Cons <$> f x'

main :: IO ()
main = do
  let l = Cons 1 $ Cons 2 $ Cons 3 Nil
  let l' :: Maybe (List Int); l' = traverse (\x -> if x == 4 then Nothing else Just (x + 1)) l
  print l'

