module Test where

import Data.List (intersperse)

digitToWord :: Int -> String
digitToWord 1 = "one"
digitToWord 2 = "two"
digitToWord 3 = "three"
digitToWord 4 = "four"
digitToWord 5 = "five"
digitToWord 6 = "six"
digitToWord 7 = "seven"
digitToWord 8 = "eight"
digitToWord 9 = "nine"
digitToWord 0 = "zero"
digitToWord _ = undefined

digits :: Int -> [Int]
digits nums = digits' nums []
  where
    digits':: Int -> [Int] -> [Int]
    digits' 0 acc = acc
    digits' ns acc = let (initNums, lastNum) = ns `divMod` 10
                      in digits' initNums (lastNum : acc)

wordNumber :: Int -> String
wordNumber = concat . intersperse "-" . map digitToWord . digits

main :: IO ()
main = let given = wordNumber 12324546
           expected = "one-two-three-two-four-five-four-six"
        in if (given == expected) then putStrLn "Nice!" else error "wuuuut?!"
