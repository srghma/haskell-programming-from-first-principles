module Test where

mc91 :: (Ord a, Num a) => a -> a
mc91 x
  | x > 100 = x - 10
  | otherwise = mc91 $ mc91 $ x + 11

main :: IO ()
main = let given = map mc91 [95..110]
           expected = [91,91,91,91,91,91,91,92,93,94,95,96,97,98,99,100]
        in if (given == expected) then putStrLn "WuUUHU!" else error "wuuuut?!"
