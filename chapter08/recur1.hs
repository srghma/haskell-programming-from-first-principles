module Test where

recursive_sum :: Integer -> Integer
recursive_sum num
  | num == 0 = 0
  | otherwise = num + recursive_sum (num - 1)

main :: IO ()
main = if (recursive_sum 5) == 15
          then putStrLn "Awesome!"
          else error "asdf"

