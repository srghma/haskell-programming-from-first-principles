module HspecExceptions where

import           Control.Exception
import           Data.Typeable     (Typeable)

type Selector a = (a -> Bool)

data MyException = ThisException | ThatException
    deriving (Show, Typeable)

instance Exception MyException

anyMyException :: Selector MyException
anyMyException = const True

main :: IO ()
main = do
  print $ anyMyException ThisException
  print $ anyMyException SomeException
