module SemVerParser where

import           Control.Applicative
import           Control.Exception.Safe       (Exception, MonadThrow, throwM)
import           Data.Typeable                (Typeable)
import           Test.Hspec
import           Text.PrettyPrint.ANSI.Leijen as Leijen (displayS, linebreak,
                                                         renderPretty, (<>))
import           Text.Trifecta
import           Text.Trifecta.Delta          (Delta)

type NumberOrString =
  Either Integer String

type Major = Integer
type Minor = Integer
type Patch = Integer
type Release = [NumberOrString]
type Metadata = [NumberOrString]
data SemVer =
  SemVer Major Minor Patch Release Metadata
  deriving (Show, Eq, Ord)

parseNos :: Parser NumberOrString
parseNos
    = (Left  <$> integer)
  <|> (Right <$> some letter) -- 1 or more

parseSemVer :: Parser SemVer
parseSemVer = SemVer <$> (integer <?> "major")
                     <*  dot
                     <*> (integer <?> "minor")
                     <*  dot
                     <*> (integer <?> "patch")
                     <*  optional (char '-')
                     <*> (nos <?> "release")
                     <*  optional (char '+')
                     <*> (nos <?> "metadata")
  where
    nos :: Parser [NumberOrString]
    nos = parseNos `sepBy` char '.' -- 0 or more


-- ├────────────────────────────────────────────────────────────────────
shouldParse :: (Show a, Eq a) => Result a -> a -> Expectation
shouldParse result expected =
  case result of
    Success a       -> a `shouldBe` expected
    Failure errInfo -> expectationFailure errMsg
      where errMsg = ($ "") $ displayS $ renderPretty 0.8 80 $ _errDoc errInfo Leijen.<> linebreak

testParse :: (Show a, Eq a, Typeable a) => Parser a -> (String, a) -> Spec
testParse parser (target, expectation) =
  let descr = target ++ " = " ++ show expectation
  in it descr $ parseString parser mempty target `shouldParse` expectation

main :: IO ()
main = hspec $ do
  testParse parseSemVer `mapM_` [ ("2.1.1", SemVer 2 1 1 [] [])
                                , ("1.0.0-x.7.z.92", SemVer 1 0 0 [Right "x", Left 7, Right "z", Left 92] [])
                                ]

  it "ord" $ SemVer 2 1 1 [] [] > SemVer 2 1 0 [] [] `shouldBe` True
