-- TODO make ini parser
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}

module Text.Fractions where

import           Control.Applicative
import           Control.Exception            (evaluate)
import           Control.Exception.Safe       (Exception, MonadThrow, SomeException, throwM)
import           Control.Monad                (join)
import           Control.Monad.IO.Class       (MonadIO, liftIO)
import           Control.Monad.Trans.Class    (lift)
import           Data.Function                ((&))
import           Data.Ratio                   ((%))
import           Data.Typeable                (Typeable)
import           System.IO
import           Test.Hspec
import           Text.Parser.Token.Highlight
import           Text.PrettyPrint.ANSI.Leijen hiding (char, integer, (<$>))
import           Text.Trifecta
import           Text.Trifecta.Delta          (Delta)
import           Text.Trifecta.Result


parseFraction :: Parser Rational
parseFraction = do
  numerator <- decimal
  char '/'
  denominator <- decimal
  case denominator of
    0 -> fail "Denominator cannot be zero"
    _ -> return (numerator % denominator)

integerIfEof :: Parser Integer
integerIfEof = (highlight Identifier integer <?> "integer") <* eof

------------------------------------
type NumberOrString =
  Either Integer String

parseNos :: Parser NumberOrString
parseNos
    = (Left <$> integer)
  <|> (Right <$> some letter) -- 1 or more

------------------------------------
type RationalOrInteger =
  Either Rational Integer

parseRoi :: Parser RationalOrInteger
parseRoi
    = (Left <$> parseFraction)
  <|> (Right <$> integer)

------------------------------------
newtype ParseException = ParseException ErrInfo
  deriving (Typeable)

instance Show ParseException where
  show (ParseException ei) = ($ "") $ displayS $ renderPretty 0.8 80 $ _errDoc ei <> linebreak

instance Exception ParseException

anyParseException :: Selector ParseException
anyParseException = const True

parseStringMaybe :: (MonadThrow m, Typeable a) =>
                    Parser a -> Delta -> String -> m a
parseStringMaybe p delta str = do
  result <- pure $ parseString p delta str
  case result of
   Success a  -> return a
   Failure xs -> throwM $ ParseException xs

testParse :: (Show a, Eq a, Typeable a) => Parser a -> (String, Maybe a) -> Spec
testParse parser (target, expectation) =
  let descr = target ++ " = " ++ show expectation
  in it descr $ parseStringMaybe parser mempty target `shouldBe` expectation

main :: IO ()
main = hspec $ do
  describe "parseFraction" $ do
    testParse parseFraction `mapM_` [ ("10", Nothing )
                                    , ("1/2", Just (1 % 2) )
                                    , ("2/1", Just (2 % 1))
                                    , ("1/0", Nothing)
                                    ]

    it "badFraction" $
      -- evaluate (1 `div` 1) `shouldThrow` anyArithException
      parseStringMaybe parseFraction mempty "1/0" `shouldThrow` anyParseException

  describe "integerIfEof" $ do
    testParse integerIfEof `mapM_` [ ("123abc", Nothing )
                                   , ("123", Just 123)
                                   ]

  describe "parse NumberOrString" $ do
    let a = "blah"
        b = "123"
        c = "123blah789"
    testParse parseNos (a, Just (Right "blah"))
    testParse (many parseNos) (c, Just [Left 123,Right "blah",Left 789])
    testParse (some parseNos) (c, Just [Left 123,Right "blah",Left 789])
