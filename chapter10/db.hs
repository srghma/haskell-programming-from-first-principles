-- cd chapter10
-- ghc -fhpc -o db db.hs
-- ./db
-- hpc marcup db.tix
module Main where

import           Data.Bool
import           Data.Time
import           Test.Hspec

data DatabaseItem =
    DbString String
  | DbNumber Integer
  | DbDate UTCTime
  deriving (Eq, Ord, Show)

theDatabase :: [DatabaseItem]
theDatabase =
  [ DbDate (UTCTime (fromGregorian 1911 5 1) (secondsToDiffTime 34123))
  , DbNumber 9001
  , DbString "Hello, world!"
  , DbDate (UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123))
  ]

filterDbDate :: [DatabaseItem] -> [UTCTime]
filterDbDate = foldr filterDbDate' []
  where
    filterDbDate' item acc = case item of
                DbDate time -> time : acc
                _           -> acc

filterDbNumber :: [DatabaseItem] -> [Integer]
filterDbNumber = foldr filterDbNumber' []
  where
    filterDbNumber' item acc = case item of
                DbNumber num -> num : acc
                _            -> acc


mostRecent :: [DatabaseItem] -> UTCTime
-- mostRecent = maximum . filterDbDate
-- or
mostRecent = foldl1 (\prev next -> bool prev next (prev < next)) . filterDbDate

sumDb :: [DatabaseItem] -> Integer
sumDb = sum . filterDbNumber

avgDb :: [DatabaseItem] -> Double
avgDb db = let nums = filterDbNumber db
               s    = sum nums
               len  = length nums
            in fromIntegral s / fromIntegral len

main :: IO ()
main = hspec $ do
  describe "filterDbDate" $ do
    it "correct" $ do
      let given = filterDbDate theDatabase
          expected =
            [ UTCTime (fromGregorian 1911 5 1) (secondsToDiffTime 34123)
            , UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123)
            ]
       in given `shouldBe` expected

  describe "filterDbNumber" $ do
    it "correct" $ do
      let given = filterDbNumber theDatabase
          expected = [ 9001 ]
       in given `shouldBe` expected

  describe "mostRecent" $ do
    it "correct" $ do
      let given = mostRecent theDatabase
          expected = UTCTime (fromGregorian 1921 5 1) (secondsToDiffTime 34123)
       in given `shouldBe` expected

  describe "sumDb" $ do
    it "correct" $ do
      let given = sumDb theDatabase
          expected = 9001
       in given `shouldBe` expected

  describe "avgDb" $ do
    it "correct" $ do
      let given = avgDb theDatabase
          expected = 9001
       in given `shouldBe` expected
