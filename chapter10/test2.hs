module Main where

import           Test.Hspec

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = foldr ((||) . f) False
-- or
-- myAny f = foldr
--           (\x acc ->
--             if f x == True
--                then True
--                else acc) False

main :: IO ()
main = hspec $ do
  describe "filterDbDate" $ do
    it "correct" $ do
      let given = myAny even [1, 2, 3]
          expected = True
       in given `shouldBe` expected
