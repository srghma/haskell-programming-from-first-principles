module Main where

import           Test.Hspec

factorial :: [Integer]
factorial = scanl (*) 1 [1..]

factorialN :: Int -> Integer
factorialN n = factorial !! n

main :: IO ()
main = hspec $ do
  describe "factorial" $ do
    it "correct" $ do
      let given = factorialN 9
          expected = 362880
       in given `shouldBe` expected
