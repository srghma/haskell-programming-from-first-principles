module My where

g :: String -> Bool -> String
g = (\acc x -> acc ++ show x)

f :: String
f = foldl g True [True, False]
