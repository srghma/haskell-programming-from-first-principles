module PersonDogReader where

import Control.Monad.Reader
import Test.Hspec

newtype HumanName =
  HumanName String
  deriving (Eq, Show)

newtype DogName =
  DogName String
  deriving (Eq, Show)

newtype Address =
  Address String
  deriving (Eq, Show)

data Person =
  Person {
    humanName :: HumanName
  , dogName :: DogName
  , address :: Address
  } deriving (Eq, Show)

data Dog =
  Dog {
    dogsName :: DogName
  , dogsAddress :: Address
  } deriving (Eq, Show)

getDog :: Person -> Dog
getDog p =
  Dog (dogName p) (address p)

-- -- with Reader
-- getDogR :: Person -> Dog
-- getDogR =
--   Dog <$> dogName <*> address

-- -- with Reader, alternate
-- getDogR' :: Person -> Dog
-- getDogR' =
--   liftA2 Dog dogName address

getDogR :: Reader Person Dog
getDogR = do
  dogName' <- asks dogName
  address' <- asks address
  return (Dog dogName' address')

main :: IO ()
main = hspec $ do
  let pers = Person (HumanName "Big Bird")
            (DogName "Barkley")
            (Address "Sesame Street")
      chris = Person (HumanName "Chris Allen")
              (DogName "Papu")
              (Address "Austin")
      persDog = Dog
                (DogName "Barkley")
                (Address "Sesame Street")
      chrisDog = Dog
                (DogName "Papu")
                (Address "Austin")
  it "no reader" $ do
    getDog pers  `shouldBe` persDog
    getDog chris `shouldBe` chrisDog
  it "reader" $ do
    runReader getDogR pers  `shouldBe` persDog
    runReader getDogR chris `shouldBe` chrisDog
